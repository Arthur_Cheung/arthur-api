var Xray = require('x-ray');
var xray = Xray();
var Promise = Promise || require('bluebird');

function scrape(url, selector, fields){
    console.info('Scraping: ', url);
    console.info('Selector: ', selector);
    console.info('Fields:', fields);
    return new Promise(function(resolve, reject){
        xray(url, selector, fields)(function(error, result){
            if(!!error){
                console.error('Failed\n');
                reject(error);
            }
            else{
                console.info('Successful\n');
                resolve(result);
            }
        });
    });
}
module.exports = scrape;

