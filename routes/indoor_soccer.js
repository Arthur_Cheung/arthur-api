var express = require('express');
var scrape = require('../utils/scrape.js');
var _ = require('lodash');


function netball(){
    var route = new express.Router();
    var selectUrl = [{
        "a": "",
        "href": "@href"
    }];
    var teamName = 'zergs';
    var sport = 'Men\'s Soccer';
    var competition = 'Thursday Evening Men\'s Soccer St Andrews';

    route.get('/', function(request, response){
        scrape('http://www.intrinsicsports.com.au/', 'a', selectUrl).then(function getSportUrl(results){
            return _(results).find({"a": sport}).href;
        }).then(function scrapeNextPage(url){
            return scrape(url, '#fixTures a', selectUrl)
        }).then(function getUrl(results){
            return results;
        }).then(function getCompetitionUrl(results){
            return _(results).find({"a": competition}).href;
        }).then(function scrapeNextPage(url){
            return scrape(url, '.fxtBlock.active ul', [{
                "teamOne": ".fxtTL > span",
                "teamOneScore": ".fxtTLS",
                "time": ".fxtT",
                "teamTwoScore": ".fxtTDS",
                "teamTwo": ".fxtTD > span"
            }]);
        }).then(function findOurGames(fixtures){
            var ourGames = _(fixtures).filter(function(fixture){
                return fixture.teamOne.toLowerCase() === teamName || fixture.teamTwo.toLowerCase() === teamName;
            }).value();
            var output = {};
            console.log(ourGames);
            var lastWeek = ourGames[0];
            var thisWeek = ourGames[1];
            output.lastWeek = {"fixture": lastWeek.teamOne + ' vs ' + lastWeek.teamTwo, "time": lastWeek.time, "score": lastWeek.teamOneScore + ' - ' + lastWeek.teamTwoScore}
            output.thisWeek = {"fixture": thisWeek.teamOne + ' vs ' + thisWeek.teamTwo, "time": thisWeek.time}
            return output;
        }).then(function sendResponse(data){
            response.send(data);
        }).catch(function handleError(error){
            console.error(error);
            response.send(error);
        });
    });

    return route;
}


module.exports = netball();