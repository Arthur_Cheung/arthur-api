# Arthur API
## Usage

After running `npm i`, `gulp serve` to run the server. You can then do a `GET` from postman the following to test:

- `http://localhost:3000/indoor_soccer`
- `http://localhost:3000/netball`

## TODO

- Need to consolidate `indoor_soccer.js`_ and `netball.js`. The code is almost identical at the moment.