var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();

app.use(cors());
app.use(bodyParser.urlencoded({"extended": true}));
app.use(bodyParser.json());
app.use('/netball', require('./routes/netball.js'));
app.use('/indoor_soccer', require('./routes/indoor_soccer.js'));

var port = 3000;
var server = app.listen(port, function() {
    console.log("App started at: " + new Date() + " on port: " + port);
});